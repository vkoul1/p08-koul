//
//  DataLoader.swift
//  Game8
//
//  Created by Vikas Koul on 5/11/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation

class DataLoader {
    var levelData : NSDictionary!
    
    class var sharedInstance: DataLoader{
        struct Singleton{
            static let instance = DataLoader()
        }
        return Singleton.instance
    }
    
    init() {
        if let path = Bundle.main.path(forResource: "Level01", ofType: "plist"){
            if let level = NSDictionary(contentsOfFile: path){
                levelData = level
            }
        }
    }
}
