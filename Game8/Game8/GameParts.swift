//
//  GameParts.swift
//  Game8
//
//  Created by Vikas Koul on 5/10/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene{
    
    //Function to create the background for the game
    func createBackGround() -> SKNode{
        let bgNode = SKNode()
        let gap = scale * 43
        for j in 0 ... 5{
            for i in 0 ... 19{
                let bg = SKSpriteNode(imageNamed: String(format: "bg%02d", i))
                bg.anchorPoint = CGPoint(x: 0.5, y: 0)
                bg.setScale(scale)
                bg.position = CGPoint(x: self.size.width/2, y: gap * CGFloat(i))
                bgNode.addChild(bg)
            }
        }
        return bgNode
    }
    
    func createCloudBlocks() -> SKNode{
        let middleNode = SKNode()
        var anchor: CGPoint!
        var x:CGFloat!

        for i in 0 ... 9{
            let randomNumber = arc4random() % 2
            if randomNumber == 0{
                anchor = CGPoint(x: 1, y: 0.5)
                x = self.size.width
            }
            else{
                anchor = CGPoint(x: 0, y: 0.5)
                x = 0
            }
            let cNode = SKSpriteNode(imageNamed: "cloud_effect")
            cNode.anchorPoint = anchor
            cNode.position = CGPoint(x: x, y: 400*CGFloat(i))
            middleNode.addChild(cNode)
        }
        return middleNode;
    }
    
    func createCloudAtPosition(position:CGPoint,oftype type:PlatformType) -> PlatformNode{
        let node = PlatformNode()
        let pos = CGPoint(x: scale * position.x, y: position.y)
        node.position = pos
        node.name = "PlatformClouds"
        node.pType = type
        
        let cloud = SKSpriteNode(imageNamed: "clouds")
        cloud.size = CGSize(width: 60, height: 40)
        node.addChild(cloud)
        
        node.physicsBody = SKPhysicsBody(rectangleOf: cloud.size)
        node.physicsBody?.isDynamic = false
        
        node.physicsBody?.categoryBitMask = CollisionMask.cloudBlocks
        node.physicsBody?.collisionBitMask = 0
        
        return node
    }
    
    func createOrbAtPosition(position:CGPoint,oftype type:ZorbsType) -> ZorbsNode{
        let node = ZorbsNode()
        let pos = CGPoint(x: scale * position.x, y: position.y)
        node.position = pos
        node.name = "zorbs"
        node.zorbType = type
        
        var zorb: SKSpriteNode
        if type == ZorbsType.sorbs{
            zorb = SKSpriteNode(imageNamed: "sorb")
        }
        else{
            zorb = SKSpriteNode(imageNamed: "Kryptonite")
        }
        zorb.size = CGSize(width: 20, height: 20)
        node.addChild(zorb)
        
        node.physicsBody = SKPhysicsBody(circleOfRadius: zorb.size.width/2)
        node.physicsBody?.isDynamic = false
        
        node.physicsBody?.categoryBitMask = CollisionMask.zorbs
        node.physicsBody?.collisionBitMask = 0
        
        return node
    }
}
