//
//  GameScene.swift
//  Game8
//
//  Created by Vikas Koul on 5/9/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion

class GameScene: SKScene,SKPhysicsContactDelegate {
    
    var bG:SKNode!;
    var foreG:SKNode!;
    var midG : SKNode!;
    
    var scale:CGFloat!
    var startGame = SKSpriteNode(imageNamed: "start")
    
    var hero:SKNode!
    
    let motionTracker = CMMotionManager()
    
    var scoreLabel : SKLabelNode?
    var score: Int = 0 {
        didSet {
            scoreLabel?.text = "Score: \(score)"
        }
    }
    
    var currentY:Int!
    var finishLine = 0
    var gameOver = false
    
    var drape : SKNode!;
    
    override init(size:CGSize){
        super.init(size: size);
        backgroundColor = SKColor.white
        scale = self.size.width / 300
        let levelData = DataLoader.sharedInstance.levelData
        currentY = 80
        finishLine = (levelData?["EndOfLevel"]! as AnyObject).integerValue
        //Creating background here
        bG = createBackGround()
        //Adding background to the scene
        addChild(bG)
        
        //Creating clouds in the scene
        midG = createCloudBlocks()
        //Adding cloud to the scene
        addChild(midG)
        
        foreG = SKNode()
        addChild(foreG)
        //Adding Hero to the game
        hero = createHero()
        foreG.addChild(hero)
        
        drape = SKNode()
        startGame.position = CGPoint(x: self.size.width/2, y: 180)
        drape.addChild(startGame)
        
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel?.text = "Score: 0"
        scoreLabel?.fontColor = UIColor.black
        scoreLabel?.position = CGPoint(x:70 ,y: self.size.height - 30)
        scoreLabel?.fontSize = 26
        foreG.addChild(scoreLabel!)
        
        //Adding clouds and zorbs
        let platforms = levelData?["Platforms"] as! NSDictionary
        let pPatterns = platforms["Patterns"] as! NSDictionary
        let pPos = platforms["Positions"] as! [NSDictionary]
        
        for pP in pPos{
            let x = (pP["x"] as AnyObject).floatValue
            let y = (pP["y"] as AnyObject).floatValue
            let pattern = pP["pattern"] as! NSString
            
            let pPattern = pPatterns[pattern] as! [NSDictionary]
            for ppPoint in pPattern{
                let xVal = (ppPoint["x"] as AnyObject).floatValue
                let yVal = (ppPoint["y"] as AnyObject).floatValue
                let type = PlatformType(rawValue: (ppPoint["type"]!  as AnyObject).integerValue)
                
                let xPos = CGFloat(x! + xVal!)
                let yPos = CGFloat(y! + yVal!)
                
                let pNode = createCloudAtPosition(position: CGPoint(x:xPos,y:yPos), oftype: type!)
                foreG.addChild(pNode)
                
                var x = randomNumber(MIN: 0, MAX: 10);
                if(x % 5 == 0){
                    let otype = ZorbsType(rawValue: (ppPoint["type"]! as AnyObject).integerValue)
                    let oNode = createOrbAtPosition(position: CGPoint(x:xPos-100,y:yPos), oftype: otype!)
                    foreG.addChild(oNode)
                }
                if(x % 3 == 0){
                    let otype = ZorbsType(rawValue: (ppPoint["type"]! as AnyObject).integerValue)
                    let oNode = createOrbAtPosition(position: CGPoint(x:xPos-60,y:yPos+20), oftype: otype!)
                    foreG.addChild(oNode)
                }
                if(x % 2 == 0){
                    let otype = ZorbsType(rawValue: (ppPoint["type"]! as AnyObject).integerValue)
                    let oNode = createOrbAtPosition(position: CGPoint(x:xPos-30,y:yPos), oftype: otype!)
                    foreG.addChild(oNode)
                }
                else{
                    let otype = ZorbsType(rawValue: (ppPoint["type"]! as AnyObject).integerValue)
                    let oNode = createOrbAtPosition(position: CGPoint(x:xPos-30,y:yPos-30), oftype: otype!)
                    foreG.addChild(oNode)
                }
            }
        }
        
        //Adding Gravity to world
        physicsWorld.gravity = CGVector(dx: 0, dy: -2)
        physicsWorld.contactDelegate = self
        
        motionTracker.accelerometerUpdateInterval = 0.2
        motionTracker.startAccelerometerUpdates(to: OperationQueue.current!) { (data:CMAccelerometerData?, error:Error?) -> Void in
            if let acceloData = data {
                let acc = acceloData.acceleration
                self.xScale = (CGFloat(acc.x)*0.75 + (self.xScale * 0.25))
            }
        }
    }
    
    func randomNumber(MIN: Int, MAX: Int)-> Int{
        return Int(arc4random_uniform(UInt32(MAX)) + UInt32(MIN));
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didSimulatePhysics() {
        hero.physicsBody?.velocity = CGVector(dx: xScale * 400, dy: hero.physicsBody!.velocity.dy)
        if hero.position.x < -20{
            hero.position = CGPoint(x: self.size.width + 20, y: hero.position.y)
        }
        else if (hero.position.x > self.size.width + 20){
            hero.position = CGPoint(x: self.size.width - 20, y: hero.position.y)
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var node:SKNode!
        score = score + 10;
        if contact.bodyA.node != hero{
            node = contact.bodyA.node
        }
        else{
            node = contact.bodyB.node
        }
        (node as! GNode).collideWithPlayer(hero: hero)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hero.physicsBody?.isDynamic = true
        hero.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 30))
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        if gameOver {
            return
        }
        
        foreG.enumerateChildNodes(withName: "PlatformClouds"){ (node, stop) -> Void in
            let cClouds = node as! PlatformNode
            cClouds.removeNode(yPos: self.hero.position.y)
        }
        
        foreG.enumerateChildNodes(withName: "zorbs"){ (node, stop) -> Void in
            let cClouds = node as! ZorbsNode
            cClouds.removeNode(yPos: self.hero.position.y)
        }
        
        if hero.position.y > 200{
            bG.position = CGPoint(x: 0, y: -(hero.position.y - 200)/10)
            midG.position = CGPoint(x: 0, y: -(hero.position.y - 200)/4)
            foreG.position = CGPoint(x: 0, y: -(hero.position.y - 200))
        }
        if Int(hero.position.y) > finishLine{
            endGame()
        }
        if Int(hero.position.y) < currentY - 800{
            endGame()
        }
    }
    
    func endGame(){
        gameOver = true
        //let trans = SKTransition.fade(withDuration: 0.5)
        self.removeAllChildren()
        let end = SKSpriteNode(imageNamed: "gameover")
        end.position = CGPoint(x: self.size.width/2, y: 450)
        self.addChild(end)
    }
}
