//
//  GNode.swift
//  Game8
//
//  Created by Vikas Koul on 5/11/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import UIKit
import SpriteKit

struct CollisionMask{
    static let hero:UInt32 = 0x00
    static let zorbs:UInt32 = 0x01
    static let cloudBlocks:UInt32 = 0x02
}

enum PlatformType:Int {
    case clouds = 0
    case cloud = 1
}

class GNode: SKNode {
    
    func collideWithPlayer(hero:SKNode) -> Bool{
        return false
    }
    
    func removeNode(yPos: CGFloat){
        if yPos > self.position.y + 200{
            self.removeFromParent()
        }
    }

}
