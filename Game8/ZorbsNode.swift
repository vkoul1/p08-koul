//
//  ZorbsNode.swift
//  Game8
//
//  Created by Vikas Koul on 5/11/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import SpriteKit

enum ZorbsType:Int{
    case sorbs = 0
    case korb = 1
}

class ZorbsNode: GNode {
    var zorbType: ZorbsType!
    
    override func collideWithPlayer(hero: SKNode) -> Bool {
        if zorbType == ZorbsType.sorbs{
            hero.physicsBody?.velocity = CGVector(dx: (hero.physicsBody?.velocity.dx)!, dy: 300)
        }
        if zorbType == ZorbsType.korb{
            hero.physicsBody?.velocity = CGVector(dx: (hero.physicsBody?.velocity.dx)!, dy: -100)
        }
        self.removeFromParent()
        return true
    }
    
    
}
