//
//  Hero.swift
//  Game8
//
//  Created by Vikas Koul on 5/11/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import Foundation
import SpriteKit

extension GameScene{
    
    func createHero() -> SKNode{
        let heroNode = SKNode()
        let hero = SKSpriteNode(imageNamed: "supermn")
        
        hero.size = CGSize(width:80 , height:80)
        
        heroNode.position = CGPoint(x: self.size.width/2, y: 100)
        
        heroNode.addChild(hero)
        
        heroNode.physicsBody = SKPhysicsBody(circleOfRadius: hero.size.width/2)
        
        //Setting the physics body attributes for the hero
        heroNode.physicsBody?.isDynamic = false
        heroNode.physicsBody?.allowsRotation = false
        heroNode.physicsBody?.restitution = 1
        heroNode.physicsBody?.linearDamping = 0
        heroNode.physicsBody?.friction = 0
        heroNode.physicsBody?.angularDamping = 0
        heroNode.physicsBody?.usesPreciseCollisionDetection = true
        
        heroNode.physicsBody?.categoryBitMask = CollisionMask.hero
        heroNode.physicsBody?.collisionBitMask = 0
        
        heroNode.physicsBody?.contactTestBitMask = CollisionMask.zorbs | CollisionMask.cloudBlocks
        
        return heroNode
    }
}
