//
//  PlatformNode.swift
//  Game8
//
//  Created by Vikas Koul on 5/11/17.
//  Copyright © 2017 Vikas Koul. All rights reserved.
//

import SpriteKit

class PlatformNode: GNode{
    var pType:PlatformType!
    
    override func collideWithPlayer(hero: SKNode) -> Bool {
        if (hero.physicsBody?.velocity.dy)! < 0{
            hero.physicsBody?.velocity = CGVector(dx: (hero.physicsBody?.velocity.dx)!, dy: 300)
            if pType == PlatformType.clouds{
                var boom = SKSpriteNode(imageNamed: "Kapow")
                boom.size = CGSize(width: 20, height: 20)
                boom.position = CGPoint(x:self.position.x,y:self.position.y)
                self.addChild(boom)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+1.0){
                    self.removeFromParent()
                }
            }
        }
        return false
    }
}
